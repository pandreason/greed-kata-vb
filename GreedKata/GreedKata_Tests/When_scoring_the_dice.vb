﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports GreedKata


<TestClass()>
Public Class When_scoring_the_dice
    Private dice(4) As Integer
    Private scoringService As GeedKataScoringService

    <TestInitialize>
    Public Sub Initialize()
        
        scoringService = New GeedKataScoringService
    End Sub

    <TestMethod()>
    Public Sub And_there_is_a_single_one_in_the_roll_then_the_score_is_100()
        dice = {1, 0, 0, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(100, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_two_ones_in_the_roll_then_the_score_is_200()
        dice = {1, 1, 0, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(200, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_is_a_single_five_in_the_roll_then_the_score_is_50()
        dice = {5, 0, 0, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(50, result)
    End Sub

    <TestMethod()>
    Public Sub And_are_two_fives_in_the_roll_then_the_score_is_100()
        dice = {5, 5, 0, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(100, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_is_a_single_five_and_a_single_1_in_the_roll_then_the_score_is_150()
        dice = {5, 1, 0, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(150, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_is_a_single_five_and_a_2_ones_in_the_roll_then_the_score_is_250()
        dice = {1, 1, 5, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(250, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_two_fives_and_a_2_ones_in_the_roll_then_the_score_is_300()
        dice = {1, 1, 5, 5, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(300, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_ones_in_the_roll_then_the_score_is_1000()
        dice = {1, 1, 1, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(1000, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_twos_in_the_roll_then_the_score_is_200()
        dice = {2, 2, 2, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(200, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_threes_in_the_roll_then_the_score_is_300()
        dice = {3, 3, 3, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(300, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_fours_in_the_roll_then_the_score_is_400()
        dice = {4, 4, 4, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(400, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_fives_in_the_roll_then_the_score_is_500()
        dice = {5, 5, 5, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(500, result)
    End Sub

    <TestMethod()>
    Public Sub And_there_are_only_3_sixes_in_the_roll_then_the_score_is_600()
        dice = {6, 6, 6, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_1_1_1_5_1_then_the_score_is_1150()
        dice = {1, 1, 1, 5, 1}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(1150, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_2_3_4_6_2_then_the_score_is_0()
        dice = {2, 3, 4, 6, 2}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(0, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_3_4_5_3_3_then_the_score_is_350()
        dice = {3, 4, 5, 3, 3}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(350, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_1_5_1_2_4_then_the_score_is_250()
        dice = {1, 5, 1, 2, 4}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(250, result)
    End Sub

    'Begin of six dice rolls

    <TestMethod()>
    Public Sub And_the_dice_are_2_2_2_2_0_then_the_score_is_400()
        dice = {2, 2, 2, 2, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(400, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_3_3_3_3_0_then_the_score_is_900()
        dice = {3, 3, 3, 3, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(900, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_4_4_4_4_0_then_the_score_is_1600()
        dice = {4, 4, 4, 4, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(1600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_5_5_5_5_0_then_the_score_is_2500()
        dice = {5, 5, 5, 5, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(2500, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_6_6_6_6_0_then_the_score_is_3600()
        dice = {6, 6, 6, 6, 0, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(3600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_2_2_2_2_2_then_the_score_is_800()
        dice = {2, 2, 2, 2, 2, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(800, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_3_3_3_3_3_then_the_score_is_2700()
        dice = {3, 3, 3, 3, 3, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(2700, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_4_4_4_4_4_then_the_score_is_6400()
        dice = {4, 4, 4, 4, 4, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(6400, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_5_5_5_5_5_then_the_score_is_12500()
        dice = {5, 5, 5, 5, 5, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(12500, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_6_6_6_6_6_then_the_score_is_21600()
        dice = {6, 6, 6, 6, 6, 0}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(21600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_2_2_2_2_2_2_then_the_score_is_1600()
        dice = {2, 2, 2, 2, 2, 2}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(1600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_3_3_3_3_3_3_then_the_score_is_8100()
        dice = {3, 3, 3, 3, 3, 3}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(8100, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_4_4_4_4_4_4_then_the_score_is_25600()
        dice = {4, 4, 4, 4, 4, 4}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(25600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_5_5_5_5_5_5_then_the_score_is_62500()
        dice = {5, 5, 5, 5, 5, 5}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(62500, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_6_6_6_6_6_6_then_the_score_is_129600()
        dice = {6, 6, 6, 6, 6, 6}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(129600, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_2_2_3_3_4_4_then_the_score_is_1200()
        dice = {2, 2, 3, 3, 4, 4}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(800, result)
    End Sub

    <TestMethod()>
    Public Sub And_the_dice_are_1_2_3_4_5_6_then_the_score_is_1200()
        dice = {1, 2, 3, 4, 5, 6}

        Dim result = scoringService.ScoreDice(dice)

        Assert.AreEqual(1200, result)
    End Sub
End Class