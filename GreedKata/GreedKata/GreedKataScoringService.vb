﻿Public Class GeedKataScoringService

    Public Function ScoreDice(ByVal dice() As Integer) As Integer

        Dim sum As Integer = 0

        For index = 1 To 6
            Dim thisIndex = index
            Dim numberOfIndex = dice.Where(Function(x As Integer) x = thisIndex).Count()
            Dim numberOfTriplets = If(numberOfIndex \ 3 > 1, 1, numberOfIndex \ 3)
            Dim numberOfSingles = (numberOfIndex - 3)
            Dim multiplier = If(numberOfSingles * index > 1, index ^ numberOfSingles, 1)

            If (dice.Distinct.Count() = 6) Then
                sum = 1200
                Continue For
            End If

            Dim sortedArray = dice.OrderBy(Function(x) x)

            If (sortedArray(0) = sortedArray(1) And sortedArray(2) = sortedArray(3) And sortedArray(4) = sortedArray(5) _
                And sortedArray(1) <> sortedArray(2) And sortedArray(3) <> sortedArray(4)) Then
                sum = 800
                Continue For
            End If

            Select Case index
                Case 1
                    sum += numberOfTriplets * 1000
                    sum += (numberOfIndex Mod 3) * 100
                Case 5
                    sum += (numberOfTriplets * (index * 100)) * multiplier
                    If numberOfTriplets = 0 Then
                        sum += (numberOfIndex Mod 3) * 50
                    End If
                Case Else
                    sum += (numberOfTriplets * (index * 100)) * multiplier
            End Select

        Next

        Return sum

    End Function



End Class
